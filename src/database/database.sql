create database abaco with owner gerardo;

++++++ Usar la base de datos +++++++
\c abaco;

CREATE TABLE usuario(
id_usuario int not null PRIMARY KEY,
id_temary int not null,
name varchar(30),
firt_last_name varchar(30),
second_last_name varchar(30),
birthday date,
email varchar(30),
password varchar(30),
FOREIGN KEY (id_temary) REFERENCES temary(id_temary)
);

CREATE TABLE temary(
id_temary int not null PRIMARY KEY,
id_module int not null,
name varchar(30),
FOREIGN KEY (id_module) REFERENCES module(id_module)
);

CREATE TABLE module(
id_module int not null PRIMARY KEY,
id_exam int not null,
name varchar(30),
FOREIGN KEY (id_exam) REFERENCES exam(id_exam)
);


CREATE TABLE exam(
id_exam int not null PRIMARY KEY, 
id_question int not null,
score float,
FOREIGN KEY (id_question) REFERENCES question(id_question)
);

CREATE TABLE question (
id_question int not null PRIMARY KEY,
id_answer int not null,
question varchar(50),
FOREIGN KEY (id_answer) REFERENCES answer(id_answer)
);

CREATE TABLE answer(
id_answer int not null PRIMARY KEY,
answer varchar(30),
validate varchar(30)
);


*****ver tablas *****
\dt;


